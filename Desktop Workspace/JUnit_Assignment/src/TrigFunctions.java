import java.util.Scanner;

public class TrigFunctions {
	static double pi = 3.14159;	//Set a class-scoped value for pi to 5 decimals
	
//Main method to handle user input, input types, etc.
	public static void main(String[] args) {
			char inType = 'a';	//Variable for degree or radian input
			double deg, rad;	//Double values for degrees and radian inputs
			String degrees, radian;	//Variables for user input
			Scanner inputType = new Scanner(System.in);	
			Scanner measure = new Scanner(System.in);
		
			//While input is not set to degrees or radians, repeat until d or r is entered
			while (inType != 'd' && inType != 'r'){	
				System.out.print("Would you like to input degrees or radians (d or r)?  ");
				inType = inputType.next().charAt(0);
			}
			
			if (inType == 'd'){	//If user wants input in degrees
				do
				{ 
					System.out.print("What is the value in degrees (__�)?  ");
					degrees = measure.next();
					//Try-catch block to ensure an input of double type is input
				    try {
				    	deg = Double.parseDouble(degrees);	//Check that input can be converted to a double type and assign to deg
				        break;
				     }
				     catch (Exception e){}
				}
				while (true);
				
				double roundedDeg = round(deg);	//Round deg to 2 decimal places using round() method
				double roundedSin = round(sin(inType, deg));
				double roundedCos = round(cos(inType, deg));
				double roundedTan = round(tan(inType, deg));
				System.out.println(tan(inType, deg));
				System.out.println("The sine of " + roundedDeg + "Pi is " + roundedSin);
				System.out.println("The cosine of " + roundedDeg + "Pi is " + roundedCos);
				if (roundedTan > 9999 || roundedTan < -9999)
					System.out.println("The tangent of " + roundedDeg + "Pi is NaN");
				else
					System.out.println("The tangent of " + roundedDeg + "Pi is " + roundedTan);
			}
			
			else if (inType == 'r'){	//If user wants input in radians
				do
				{ 
					System.out.print("What is the value in radians (__Pi)?  ");
			    	radian = measure.next();
			    	//Try-catch block to ensure an input of double type is input
				    try {
				    	rad = Double.parseDouble(radian);	//Check that input can be converted to a double type and assign to rad
				        break;
				    }
				    catch (Exception e){}
				}
				while (true);
				
				double roundedRad = round(rad);	//Round rad to 2 decimal places using round() method
				double roundedSin = round(sin(inType, rad));
				double roundedCos = round(cos(inType, rad));
				double roundedTan = round(tan(inType, rad));
				System.out.println("The sine of " + roundedRad + "Pi is " + roundedSin);
				System.out.println("The cosine of " + roundedRad + "Pi is " + roundedCos);
				if (roundedTan == 9999 || roundedTan == -9999)
					System.out.println("The tangent of " + roundedRad + "Pi is NaN");
				else
					System.out.println("The tangent of " + roundedRad + "Pi is " + roundedTan);
			}
			
			inputType.close();
			measure.close();
			System.out.println("Complete");
	}
	
//Method to compute powers with positive exponents
	public static double pow(double x, int y){
		double result = 1;
		for (int i=1; i<=y; i++)	//Repeat y number of times that the base is raised to the exponent of
			result *= x;	//Multiply the result my the base
		return result;
	}
	
//Method to round to a maximum of 2 decimal places
	static double round(double z){
		double temp = z, newZ = z;	//temp will be used to determine whether to round up or down
		double round = 0;	//round will be the amount +/- in order to fulfill the rounding
		temp *= 1000;	//Multiply by 1000 to move the third decimal to the one's position
		temp %= 10;	//Mod by 10 to get the digit in the third decimal position (now the one's position)
		newZ *= 1000;
		if (temp >= 5){	//If the third decimal is >= 5, round up
			round = 10 - temp;	//Determine how much lower than 10 the third decimal digit is
			newZ += round;	//Add this value to newZ to 'round up'
			newZ = (int)newZ;	//Convert to an interger to remove all trailing decimals
			newZ /= 1000;	//Divide by 1000 to convert to a 2 decimal number 
		}
		else{	//Otherwise, round down
			round = temp;	//Set round to the decimal digit's value
			newZ -= round;	//Subtract this value to newZ to 'round down'
			newZ = (int)newZ;	//Convert to an interger to remove all trailing decimals
			newZ /= 1000;	//Divide by 1000 to convert to a 2 decimal number 
		}
		return newZ;
	}
	
//Method to calculate the sine value given either degrees or radians
	public static double sin(char type, double measure){
		double sinVal = 0, radians = measure;	//radians is used as for all calculations
        int n = 10, i, j;	//Use n=10 so to keep the factorials computable
        long fac;
        
        if (type == 'd'){	//If input was in degrees
        	radians %= 360;	//Mod by 360 to keep the value between 0 and 360
        	radians = radians*pi/180;	//Convert the degrees value to radians
        	}
        else if (type == 'r'){	//If input was in radians
        	radians %= 2;	//Mod by 2 to keep the value between 0Pi and 2Pi
        	radians = radians*pi;	//Multiply the radian constant by Pi
        }
        
        for(i=0; i<=n; i++){	//Loop for 10 iterations
            fac = 1;
            for(j=2; j<=2*i+1; j++)	//Loop for factorials
                fac*=j;	//Multiply next value into factorial
            sinVal += pow(-1.0,i)*pow(radians,2*i+1)/fac;	//Taylor series calculation for sine
        }
        return sinVal;
	}
	
//Method to calculate the cosine value given either degrees or radians
	public static double cos(char type, double measure){
		double cosVal = 0, radians = measure;	//radians is used as for all calculations
        int n = 10, i, j;	//Use n=10 so to keep the factorials computable
        long fac;
        
        if (type == 'd'){	//If input was in degrees
        	radians %= 360;	//Mod by 360 to keep the value between 0 and 360
        	radians = radians*pi/180; //Convert the degrees value to radians
        	}
        else if (type == 'r'){	//If input was in radians
        	radians %= 2;	//Mod by 2 to keep the value between 0Pi and 2Pi
        	radians = radians*pi;	//Multiply the radian constant by Pi
        }
        
        for(i=0; i<=n; i++){	//Loop for 10 iterations
            fac = 1;
            for(j=2; j<=2*i; j++)	//Loop for factorials
                fac*=j;	//Multiply next value into factorial
            cosVal += pow(-1.0,i)*pow(radians,2*i)/fac;	//Taylor series calculation for cosine
        }
        return cosVal;
	}

//Method to calculate the tangent value given either degrees or radians
	public static double tan(char type, double measure){
		double sine = sin(type, measure);
		double cosine = cos(type, measure);
		double tanVal = sine/cosine;	//Tangent identity
		return tanVal;
	}
}
