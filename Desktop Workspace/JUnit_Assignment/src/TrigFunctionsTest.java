import static org.junit.Assert.*;

import org.junit.*;

public class TrigFunctionsTest {
	
	TrigFunctions tester = new TrigFunctions();
	
	@SuppressWarnings("static-access")
	@Test
	public void roundTest() {
		//Test the round method used for rounding to 2 decimal places
		//Test 0-3 decimals equal to 0
		assertTrue(tester.round(2) == 2.0);
		assertTrue(tester.round(2.0) == 2.0);
		assertTrue(tester.round(2.00) == 2.0);
		assertTrue(tester.round(2.000) == 2.0);
		
		//Test 1-3 decimal places with digits 1-9, 3rd decimal as round up/down, rounding multiple decimal positions
		assertTrue(tester.round(2.1) == 2.1);
		assertTrue(tester.round(2.12) == 2.12);
		assertTrue(tester.round(2.123) == 2.12);
		assertTrue(tester.round(2.115) == 2.12);
		assertTrue(tester.round(2.999) == 3.0);
	}

	@SuppressWarnings("static-access")
	@Test
	public void powTest() {
		//Test the pow method (Only computes positive exponents since Taylor series won't encounter negative)
		//Test positive even/odd
		assertTrue(tester.pow(2, 2) == 4);
		assertTrue(tester.pow(2, 3) == 8);
		assertTrue(tester.pow(3, 2) == 9);
		assertTrue(tester.pow(3, 3) == 27);
		
		//Test negative even/odd
		assertTrue(tester.pow(-2, 2) == 4);
		assertTrue(tester.pow(-2, 3) == -8);
		assertTrue(tester.pow(-3, 2) == 9);
		assertTrue(tester.pow(-3, 3) == -27);
		
		//Test max n value & max+1
		assertTrue(tester.pow(2, 10) == 1024);
		assertTrue(tester.pow(2, 11) == 2048);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void sinSpecial() {
		//Degrees special angles
		assertTrue("sin 0", tester.sin('d', 0) == 0);
		assertTrue("Sin 90", tester.sin('d', 90) >= 0.99);	//Use -0.99 for approximation
		assertTrue("Sin 180", tester.sin('d', 180) > -0.01 && tester.sin('d', 180) < 0.01);	//Use +/- 0.01 for approximation
		assertTrue("Sin 270", tester.sin('d', 270) <= -0.99);	//Use - 0.99 for approximation
		assertTrue("Sin 360", tester.sin('d', 360) == 0);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void sinQuadrants() {
		//Degrees 4 quadrants
		assertTrue("Sin Q1", tester.sin('d', 45)>0 && tester.sin('d', 45)<1);
		assertTrue("Sin Q2", tester.sin('d', 135)>0 && tester.sin('d', 135)<1);
		assertTrue("Sin Q3", tester.sin('d', 225)<0 && tester.sin('d', 225)>-1);
		assertTrue("Sin Q4", tester.sin('d', 315)<0 && tester.sin('d', 315)>-1);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void sinOuterBounds() {
		//Degrees less than 0 and greater than 360 (Outside lower and upper bound)
		assertTrue("Sin Lower", tester.sin('d', -315)>0 && tester.sin('d', -315)<1);
		assertTrue("Sin Upper", tester.sin('d', 405)>0 && tester.sin('d', 405)<1);
	}

	@SuppressWarnings("static-access")
	@Test
	public void cosSpecial() {
		//Degrees special angles
		assertTrue("Cos 0", tester.cos('d', 0) == 1);
		assertTrue("Cos 90", tester.cos('d', 90) >= -0.01 && tester.cos('d', 90) <= 0.01);	//Use +/- 0.01 for approximation
		assertTrue("Cos 180", tester.cos('d', 180) <= -0.99 );	//Use - 0.99 for approximation
		assertTrue("Cos 270", tester.cos('d', 270) >= -0.01 && tester.cos('d', 270) <= 0.01);	//Use +/- 0.01 for approximation
		assertTrue("Cos 360", tester.cos('d', 360) == 1);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void cosQuadrants() {
		//Degrees 4 quadrants
		assertTrue("Cos Q1", tester.cos('d', 45)>0 && tester.cos('d', 45)<1);
		assertTrue("Cos Q2", tester.cos('d', 135)<0 && tester.cos('d', 135)>-1);
		assertTrue("Cos Q3", tester.cos('d', 225)<0 && tester.cos('d', 225)>-1);
		assertTrue("Cos Q4", tester.cos('d', 315)>0 && tester.cos('d', 315)<1);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void cosOuterBounds() {
		//Degrees less than 0 and greater than 360 (Outside lower and upper bound)
		assertTrue("Cos Lower", tester.cos('d', -315)>0 && tester.cos('d', -315)<1);
		assertTrue("Cos Upper", tester.cos('d', 405)>0 && tester.cos('d', 405)<1);
	}

	@SuppressWarnings("static-access")
	@Test
	public void tanSpecial() {
		//Degrees special angles
		assertTrue("Tan 0", tester.tan('d', 0) == 0);
		assertTrue("Tan 90", tester.tan('d', 90) < -9999 || tester.tan('d', 90) > 9999);	//Use +/- 9999 for approaching infinity
		assertTrue("Tan 180", tester.tan('d', 180) <= 0.01 && tester.tan('d', 180) >= -0.01);
		assertTrue("Tan 270", tester.tan('d', 270) < -9999 || tester.tan('d', 270) > 9999);	//Use +/- 9999 for approaching infinity
		assertTrue("Tan 360", tester.tan('d', 360) == 0);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void tanQuadrants() {
		//Degrees 4 quadrants
		assertTrue("Tan Q1", tester.tan('d', 45)>0);
		assertTrue("Tan Q2", tester.tan('d', 135)<0);
		assertTrue("Tan Q3", tester.tan('d', 225)>0);
		assertTrue("Tan Q4", tester.tan('d', 315)<0);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void tanOuterBounds() {
		//Degrees less than 0 and greater than 360 (Outside lower and upper bound)
		assertTrue("Tan Lower", tester.tan('d', -315)>0);
		assertTrue("Tan Upper", tester.tan('d', 405)>0);
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void radianInputs() {
		//Radian input for each function, since they all convert degrees to radians 
		//we only need to test one initial radian input for each
		assertTrue("Sin Radian Input", tester.sin('r', 2) == 0);
		assertTrue("Cos Radian Input", tester.cos('r', 2) == 1);
		assertTrue("Tan Radian Input", tester.tan('r', 2) == 0);
	}
}
