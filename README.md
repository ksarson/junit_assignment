# JUnit_Assignment

Assignment 2b for COMP 4110 - Java trig library with JUnit test suite.

Version 1 - Contains only the main and round methods, with empty sin/cos/tan methods & failed sin degrees tests

Version 2 - Adds a pow and sin method with degrees to radians conversion, with successful sin degrees tests and failed cos degrees tests

Version 3 - Adds a cos method with degrees to radians, with successful cos degrees tests and failed tan degrees tests

Version 4 - Adds a tan method based on the identity (tan=sin/cos), with successful tan degrees tests

Version 5 - Adds additional tests for a radian input for each of sin, cos, and tan

Version 6 - Adds tests for pow and round methods